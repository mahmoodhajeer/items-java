package com.opensooq.opensooq.model;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}
