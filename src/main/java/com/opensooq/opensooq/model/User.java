package com.opensooq.opensooq.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import jdk.jfr.Timestamp;

import java.time.LocalDateTime;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "users", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"nId"}),
		@UniqueConstraint(columnNames = {"email"})
})

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @NumberFormat
    @Column(nullable = false)
    private int nId;

    @Email
    @Column(nullable = false)
    private String email;

    @NumberFormat
    private int phoneNumber;

    @NumberFormat
    @Column(nullable = false)
    private int sex;

    private String imageUrl;

    @Column(nullable = false)
    private Boolean emailVerified = false;

    @JsonIgnore
    private String password;

    @NotNull
    @Enumerated(EnumType.STRING)
    private AuthProvider provider;

    private String providerId;
    
    @NotNull
    @Column(name="created",columnDefinition="TIMESTAMP", nullable = false)      
    private LocalDateTime created;

    @NotNull
    @Column(name="updated",columnDefinition="TIMESTAMP", nullable = false)      
    private LocalDateTime updated;

    @NotNull
    @NumberFormat
    @Column(nullable = false)
    private int status;


    public Long getId(){
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }


    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }


    public int getNId(){
        return nId;
    }
    public void setNId(int nId){
        this.nId = nId;
    }


    public String getEmail(){
        return email;
    }
    public void setEmail(String email){
        this.email = email;
    }


    public int getPhoneNumber(){
        return phoneNumber;
    }
    public void setPhoneNumber(int phoneNumber){
        this.phoneNumber = phoneNumber;
    }


    public int getSex(){
        return sex;
    }
    public void setSex(int sex){
        this.sex = sex;
    }


    public String getImageUrl(){
        return imageUrl;
    }
    public void setImageUrl(String imageUrl){
        this.imageUrl = imageUrl;
    }


    public boolean getEmailVerified(){
        return emailVerified;
    }
    public void setEmailVerified(boolean emailVerified){
        this.emailVerified = emailVerified;
    }


    public String getPassword(){
        return password;
    }
    public void setPassword(String password){
        this.password = password;
    }


    public AuthProvider getProvider(){
        return provider;
    }
    public void setProvider(AuthProvider provider){
        this.provider = provider;
    }


    public String getProviderId(){
        return providerId;
    }
    public void setProviderId(String providerId){
        this.providerId = providerId;
    }


    public int getStatus(){
        return status;
    }
    public void setStatus(int status){
        this.status = status;
    }


    public LocalDateTime getCreated(){
        return created;
    }
    public void setCreated(LocalDateTime created){
        this.created = created;
    }



    public LocalDateTime getUpdated(){
        return updated;
    }
    public void setUpdated(LocalDateTime updated){
        this.updated = updated;
    }



}
























